#include <stdio.h>
#include <stdlib.h>

void jdl_aplikasi() {
   printf("*************************\n");
    printf("*  Aplikasi Hitung Gaji *\n");
    printf("*************************\n\n");
}

void input_data(char *nama, char *gol, char *status) {
    printf("Nama Karyawan       : ");
    scanf("%s", nama);
    printf("Golongan (A\\B)      : ");
    scanf(" %c", gol);
    printf("Satus (Nikah\\Belum) : ");
    scanf("%s", status);
}

void hitung_gaji(char gol, char *status, float *gapok, float *tunja, float *prosen_pot) {
    switch (gol) {
        case 'A':
            *gapok = 200000;
            if (strcmp(status, "Nikah") == 0) {
                *tunja = 50000;
            } else {
                if (strcmp(status, "Belum") == 0) {
                    *tunja = 25000;
                }
            }
            break;
        case 'B':
            *gapok = 350000;
            if (strcmp(status, "Nikah") == 0) {
                *tunja = 75000;
            } else {
                if (strcmp(status, "Belum") == 0) {
                    *tunja = 60000;
                }
            }
            break;
        default:
            printf(" Salah Input Golongan !!!\n");
            exit(0);
    }

    if (*gapok > 300000) {
        *prosen_pot = 0.1;
    }
}

float hitung_potongan(float gapok, float tunja, float prosen_pot) {
    return (gapok + tunja) * prosen_pot;
}

float hitung_gaji_bersih(float gapok, float tunja, float pot) {
    return (gapok + tunja) - pot;
}

void output_data(float gapok, float tunja, float pot, float gaber) {
    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);
}

int main() {
    system("clear");

    char nama[50];
    char gol = ' ';
    char status[10];

    float gapok = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    jdl_aplikasi();
    input_data(nama, &gol, status);
    hitung_gaji(gol, status, &gapok, &tunja, &prosen_pot);
    pot = hitung_potongan(gapok, tunja, prosen_pot);
    float gaber = hitung_gaji_bersih(gapok, tunja, pot);
    output_data(gapok, tunja, pot, gaber);

 return 0;
}
