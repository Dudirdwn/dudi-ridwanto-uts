#include<stdio.h>

long factorial(int n) {
    int c;
    long result = 1;

    for (c = 1; c <= n; c++)
        result = result * c;
   return result; 
}  
int main() {
    int n, coef = 1, space, i, j;

    printf("Masukan N: ");
    scanf("%d", &n);
for (i = 0; i < n; i++) {
        for (space = n - i; space > 0; space--)
            printf(" ");

        for (j = 0; j <= i; j++) {
            if (j == 0 || j == i)
                coef = 1;
            else
                coef = coef * (i - j + 1) / j;

            printf("%ld ", coef);
        }

        printf("\n");
    }

    return 0;
}
